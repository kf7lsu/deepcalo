import os
import sys
import copy
import time
from joblib import dump
import argparse
from tqdm import tqdm
from multiprocessing import Pool
from skopt import Optimizer
from skopt.space import Real, Integer, Categorical
import deepcalo as dpcal


# Define objective
def objective(variable_params, params, search_space_as_dict, exp_dir='/',
              gpu_id='', save_figs=True, verbose=True):

    os.environ['CUDA_VISIBLE_DEVICES'] = gpu_id

    # Translate from hyperparameter space to dict passed to ModelContainer
    # First merge the dicts, such that all entries (keys) in params
    # overlapping with entries in the search space (variable_params) are overwritten
    params = dpcal.utils.merge_dicts(params, variable_params)

    # This is where custom search parameters are integrated into params.
    # For instance, maybe you want to sample the mini-batch size in powers of 2,
    # by way of an integer parameter in the search space called 'log2_batch_size'.
    # This could then the translated into something ModelContainer will understand
    # as follows:
    if 'log2_batch_size' in params:
        params['batch_size'] = 2 ** params['log2_batch_size']

    # Set learning rate based on optimizer and batch size
    if params['auto_lr']:
        params = dpcal.utils.set_auto_lr(params)

    # It is currently not supported to use several GPUs from within a single process in the pool
    params['n_gpus'] = 0 if not gpu_id else len(gpu_id.split(','))

    # Make directories for saving figures and models
    dirs = dpcal.utils.create_directories(exp_dir, params['epochs'], log_prefix=gpu_id) # returns dictionary

    # Redirect output
    sys.stdout = open(dirs['log'] + 'std.out', 'w')
    sys.stderr = open(dirs['log'] + 'std_error.out', 'w')

    # Instantiate model container (with self.model in it)
    mc = dpcal.ModelContainer(data=data,
                            params=params,
                            dirs=dirs,
                            save_figs=save_figs,
                            verbose=verbose)

    # Save hyperparameters
    params_for_saving = copy.deepcopy(mc.params)
    params_for_saving['n_params'] = mc.model.count_params()
    dpcal.utils.save_dict(params_for_saving, dirs['log'] + 'hyperparams.txt', save_pkl=True)

    # Train model
    mc.train()

    # Evaluate (predicting and evaluating on test or validation set)
    if not hasattr(mc,'evaluation_scores'):
        mc.evaluate()

    # Print results
    if verbose:
        print('Evaluation scores:')
        print(mc.evaluation_scores, flush=True)

    # Use first entry in metrics list as evaluation function used by the
    # Gaussian process hyperparameter search
    score = mc.evaluation_scores[mc.model.metrics_names[-1]]

    return dpcal.utils.point_aslist(search_space_as_dict, variable_params), score



if __name__=='__main__':
    # ==========================================================================
    # Argument parsing
    # ==========================================================================
    parser = argparse.ArgumentParser()
    parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='', type=str)
    parser.add_argument('--data_path', help='Path to data. Can be relative.', default=None, type=str)
    parser.add_argument('--exp_dir', help='Directory of experiment, e.g. "my_exp/".', default='./', type=str)
    parser.add_argument('--n_calls', help='How many networks should be trained for the chosen search space.', default=int(100), type=int)
    parser.add_argument('--n_initial_points', help='How many random initial points the Gaussian process should spawn before trying to navigate the landscape based on the acquisition function.', default=int(10), type=int)
    parser.add_argument('--n_jobs', help='Number of cores to run in parallel while running the acquisition function. Set to -1 for all.', default=int(4), type=int)
    parser.add_argument('--sleep_time', help='Time in seconds in between checking if any run has finished.', default=1, type=float)
    parser.add_argument('--n_train', help='How many data points to load from the training set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
    parser.add_argument('--n_val', help='How many data points to load from the validation set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
    parser.add_argument('--n_test', help='How many data points to load from the test set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
    parser.add_argument('--save_figs', help='Save figures.', default=True, type=dpcal.utils.str2bool)
    parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
    args = parser.parse_args()

    gpu_ids = args.gpu
    data_path = args.data_path
    exp_dir = args.exp_dir
    n_calls = args.n_calls
    n_initial_points = args.n_initial_points
    n_jobs = args.n_jobs
    sleep_time = args.sleep_time
    _n_train = int(args.n_train)
    _n_val = int(args.n_val)
    _n_test = int(args.n_test)
    save_figs = args.save_figs
    verbose = args.verbose

    # Set which GPU(s) to use
    os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
    os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

    # ==========================================================================
    # Hyperparameters
    # ==========================================================================
    # Set hyperparameters that should be different from the default values
    params = {'top':{'normalization':'batch',
                     'activation':'leakyrelu',
                     'units':[32,32,1]},
              'cnn':{'normalization':'batch',
                     'activation':'leakyrelu',
                     'block_depths':[1,2,2]}}

    # Use the chosen parameters where given, and use the default parameters otherwise
    params = dpcal.utils.merge_dicts(dpcal.utils.get_default_params(), params, in_depth=True)

    # Define hyperparameter search space
    # Some (like 'loss') can be used directly in params, while others (like
    # 'log2_batch_size') requires manual integration, see in objective() above.
    search_space = [Integer(5,8, name='log2_batch_size'),
                    Categorical(['mae', 'mse', 'logcosh'], name='loss')]

    # Convert search space to dict, and save
    search_space_as_dict = {var.name:var.bounds for var in search_space}
    search_space_for_saving = dpcal.utils.merge_dicts(params, search_space_as_dict)
    dpcal.utils.save_dict(search_space_for_saving, exp_dir + 'param_search_space.txt', overwrite=True, save_pkl=True)

    # ==========================================================================
    # Data
    # ==========================================================================
    # Load the first 1000 points, except if something else was given from the
    # command line. To load all points, set to None.
    n_points = {'train':1e3, 'val':1e3, 'test':1e3}
    for set_name, n in zip(n_points, [_n_train, _n_val, _n_test]):
        if not n < 0:
            n_points[set_name] = n

    # If a datagenerator is used, load a single point for each set (e.g. train, val
    # and test) to construct the model with (the construction uses the shapes of the
    # data). The actually used number of points from each set is given by
    # params['data_generator']['n_points']
    if params['data_generator']['use']:
        params['data_generator']['n_points'] = n_points
        n_points = {set_name:1 for set_name in n_points}
        if data_path is not None:
            params['data_generator']['path'] = data_path

    # Load data
    # Here, we will use the in-built loading function, designed with ATLAS data in mind
    if params['data_generator']['use']:
        data = dpcal.utils.load_atlas_data(n_points=n_points,
                                           **params['data_generator']['load_kwargs'],
                                           verbose=False)
    else:
        data = dpcal.utils.load_atlas_data(path=data_path,
                                           n_points=n_points,
                                           target_name='p_truth_E',
                                           img_names=['em_barrel'])

    # ==========================================================================
    # Distribute hyperparameter search over GPUs
    # ==========================================================================
    # Get available GPUs
    gpu_id_list = gpu_ids.split(',')

    # For each GPU, keep track of its results, which has a ready() method,
    # telling if it has completed its given task
    gpu_state = {id:None for id in gpu_id_list}

    # Initialize optimizer, which will keep track of the results received from
    # all GPUs
    opt = Optimizer(search_space, # TODO: Add noise
                    n_initial_points=n_initial_points,
                    acq_optimizer_kwargs={'n_jobs':n_jobs})

    # Begin the process of as many workers as there are visible GPUs
    with Pool(processes=len(gpu_id_list)) as pool:

        n_completed_calls = 0
        ready_ids = [id for id in gpu_state]

        # Create progress bar
        if verbose:
            pbar = tqdm(total=n_calls)

        while n_completed_calls < n_calls:

            if len(ready_ids) > 0:

                # Update optimizer state
                n_updated = 0
                for id in ready_ids:
                    if gpu_state[id] is not None:
                        result = gpu_state[id].get()
                        if verbose:
                            if id != '':
                                pbar.write(f'GPU {id} returned {result[1]}.')
                            else:
                                pbar.write(f'CPU(s) returned {result[1]}.')
                        opt_result = opt.tell(*result)
                        n_updated += 1
                n_completed_calls += n_updated
                if verbose:
                    pbar.update(n_updated)

                # Save optimizer state and most recent results
                if n_updated > 0:
                    with open(exp_dir + 'optimizer.pkl', 'wb') as f:
                        dump(opt, f) # NOTE: Used to be pickle.dump
                    with open(exp_dir + 'results.pkl', 'wb') as f:
                        # Delete objective function before saving. To be used when results have been loaded,
                        # the objective function must then be imported from this script.
                        if opt_result.specs is not None:
                            if 'func' in opt_result.specs['args']:
                                res_without_func = copy.deepcopy(opt_result)
                                del res_without_func.specs['args']['func']
                                dump(res_without_func, f)
                            else:
                                dump(opt_result, f)
                        else:
                            dump(opt_result, f)

                # Sample points for all idle GPUs
                sampled_points = opt.ask(len(ready_ids))

                # Convert sampled points to dict
                sampled_points = [dpcal.utils.point_asdict(search_space_as_dict, point) for point in sampled_points]

                # Distribute and evaluate the sampled points over the workers
                for point,id in zip(sampled_points,ready_ids):
                    args = (point, params, search_space_as_dict, exp_dir, id, save_figs, verbose)
                    gpu_state[id] = pool.apply_async(objective, args=args)

                # Check if there are any ready (idle) GPUs
                ready_ids = [id for id in gpu_state if gpu_state[id].ready()]

            else:
                # Wait a bit, so the while-condition isn't checked constantly
                time.sleep(sleep_time)

                # Check if there are any ready (idle) GPUs
                ready_ids = [id for id in gpu_state if gpu_state[id].ready()]

        pbar.close()
